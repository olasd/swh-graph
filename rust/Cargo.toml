[package]
name = "swh-graph"
version.workspace = true
edition = "2021"
description = "Compressed in-memory representation of the Software Heritage archive graph"
repository = "https://gitlab.softwareheritage.org/swh/devel/swh-graph"
license = "GPL-3.0-or-later"
readme = "README.md"
keywords = ["software heritage", "graph", "compression", "webgraph"]
categories = ["compression", "graph"]

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
rand = "0.8.5"
anyhow = {version="1.0.71", features=["backtrace"]}
mmap-rs = "0.5.0"
sux = "0.1.1"
#sux = {path = "../../../sux-rs" }
webgraph = {git = "https://github.com/vigna/webgraph-rs", rev = "ba318291b06b61b7dd3babb4242a16b8bf83f6e7" }
#webgraph = { path = "../../../webgraph-rs" }
log = "0.4.17"
libc = "0.2.147"
stderrlog = "0.5.4"
dsi-progress-logger = "0.1.0"
clap = { version = "4.1.6", features = ["derive"] }
#ph = { git = "https://github.com/beling/bsuccinct-rs.git", rev = "f6636350a8149bc8deca83527ca195fe22311a4e" }
ph = { version = "0.8.0" }
#orcxx = { version = "0.4.2", optional = true }
#orcxx_derive = { version = "0.4.2", optional = true }
orcxx = { git = "https://gitlab.softwareheritage.org/swh/devel/orcxx-rs.git", optional = true, rev = "a8101cabab6267dad5916a4937228b71d6fe8263" }
orcxx_derive = { git = "https://gitlab.softwareheritage.org/swh/devel/orcxx-rs.git", optional = true, rev = "a8101cabab6267dad5916a4937228b71d6fe8263" }
#orcxx = { path = "../../../../rust/orcxx/orcxx/", optional = true }
#orcxx_derive = { path = "../../../../rust/orcxx/orcxx_derive/", optional = true }
faster-hex = { version = "0.8.0", features = ["std"], default-features = false }
rayon = { version = "1.7.0" }
sha1 = { version = "0.10.1", optional = true }
itertools = { version = "0.11.0" }
thread_local = "1.1.7"
tempfile = "3.7.1"
zstd = "0.12"
num_cpus = "1.16.0"
byteorder = "1.4.3"
java-properties = "1.4.1"
base64-simd = "0.8.0"
dsi-bitstream = {git = "https://github.com/vigna/dsi-bitstream-rs"}
epserde = "0.2.2"
bytemuck = "1.14.0"
common_traits = "0.10.0"
lender = "0.2.0"
serde_json = { version = "1.0", optional = true }
serde = { version = "1.0.189", optional = true }
serde_derive = { version = "1.0.189", optional = true }

# grpc
chrono = { version = "0.4.31", features = ["serde"], optional = true }
prost = { version = "0.12", optional = true }
prost-types = { version = "0.12", optional = true }
tokio = { version = "1.0", features = ["macros", "rt-multi-thread"], optional = true }
tokio-stream = "0.1"
tonic = { version = "0.10.2", optional = true }
tonic-reflection = { version = "0.10.2", optional = true }

[dev-dependencies]
bitvec = { version = "1.0.1", features = ["atomic"] }

[build-dependencies]
tonic-build = { version = "0.10.2", optional = true }

[features]
default = []

# Reads dataset from ORC files and produces a compressed graph
compression = ["orcxx", "orcxx_derive", "sha1"]

grpc-server = [
    "chrono",
    "serde",
    "serde_derive",
    "serde_json",
    "tonic",
    "tonic-build",
    "tonic-reflection",
    "prost",
    "prost-types",
    "tokio"
]

[[bin]]
name = "compress"
required-features = ["compression"]

[[bin]]
name = "grpc-serve"
required-features = ["grpc-server"]
